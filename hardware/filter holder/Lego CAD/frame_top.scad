use <LEGO.scad>;

hole_length = 70;
hole_width = 30;
hole_depth = 10;


module top(){
        color( "white" )
        
        block(
            type="tile",
            width=7,
            length=14,
            roadway_width=5,
            roadway_length=12,
            roadway_x=1,
            roadway_y=1,
            height=1/2

        );
}
 
 module frame_hole(){
   difference(){
   top(); 
   translate(v = [5-hole_length/2, -hole_width/2])
   cube(size = [hole_length,hole_width,hole_depth], center = false);
    }
}

frame_hole();