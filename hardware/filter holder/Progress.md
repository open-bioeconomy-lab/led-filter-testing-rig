## Attempt 1: Lego Version

- Developed holder for filter using [OpenScad Lego library](https://github.com/cfinke/LEGO.scad/blob/master/LEGO.scad)
- Wrote OpenSCAD code for top frame and bottom frame
- v1 required hole off-setting to accommodate punch hole in filter sample and also warped badly when pressed together so added thicker weight
- v2 (see [OpenSCAD files](https://gitlab.com/open-bioeconomy-lab/led-filter-testing-rig/-/tree/master/hardware/filter%20holder/Lego%20CAD)) still warped when fixed together and the studs were easily broken off so a new approach was needed.


## Attempt 2: Slider Version

  - Adapted from Kumichou's [OpenSCAD Sliding Lid Box with Honeycomb Grip](https://github.com/kumichou/lidded-box-with-honeycomb-grip) 
  - May need to adapt "box_width" parameter for the lid in Slide_Lid.scad to be 1.5mm less than "box_width" in Slide_Frame.scad, needs to be set per printer to get a good fit.