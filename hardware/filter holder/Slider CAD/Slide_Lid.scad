// Parameterized box generator
// Originally written by Brad Koehn
// Heavily modified by Eric Hankinson to include honeycomb grid on lid, and flatten groove to prevent pushout of box walls.
// Further modified by Jenny Molloy to create a sliding frame rather than a box.
// Requires OpenSCAD 2014.03 or newer

// box dimensions in millimeters
// typically you want the height < width < length
// Important: set the box_width to LESS than the box_width in Slide_Frame.scad. How much less will depend on your printer and the size of the print, I used 1.5mm less on a 50mm print.
box_width = 48.5; // x axis
box_length = 110;  // y axis
box_height = 5; // z axis

// hole dimensions in millimeters

hole_length = 50;
hole_width = 30;
hole_depth = 10;


// thickness of all walls and lid
wall_width = 2;

// how far from the top of the box the lid notch should go
notch_height = 4;

// how deep into the wall the lid notch should go
notch_depth = wall_width / 2;


module lid(inset = 0.2) {
    union() {
        polyhedron(
            points = [
                // top points, clockwise starting at max x, max y
                [box_width / 2 - wall_width - inset, box_length / 2, notch_depth], // 0
                [box_width / 2 - wall_width - inset, -(box_length / 2 - wall_width), notch_depth], // 1
                [-(box_width / 2 - wall_width + inset), -(box_length / 2 - wall_width), notch_depth], // 2
                [-(box_width / 2 - wall_width + inset), box_length / 2, notch_depth], // 3

                // middle points, clockwise starting at max x, max y
                [box_width / 2 - wall_width + notch_depth - inset, box_length / 2, -notch_depth], // 4
                [box_width / 2 - wall_width + notch_depth - inset, -(box_length / 2 - wall_width + notch_depth), -notch_depth], // 5
                [-(box_width / 2 - wall_width  + notch_depth + inset), -(box_length / 2 - wall_width + notch_depth), -notch_depth], // 6
                [-(box_width / 2 - wall_width  + notch_depth + inset), box_length / 2, -notch_depth], // 7

                // bottom points, clockwise starting at max x, max y
                [box_width / 2 - wall_width - inset, box_length / 2, -notch_depth], // 8
                [box_width / 2 - wall_width - inset, -(box_length / 2 - wall_width), -notch_depth], // 9
                [-(box_width / 2 - wall_width + inset), -(box_length / 2 - wall_width), -notch_depth], // 10
                [-(box_width / 2 - wall_width + inset), box_length / 2, -notch_depth], // 11
            ],
            faces = [
                [0, 1, 2, 3], // top face

                [0, 4, 5, 1], // left top notch
                [1, 5, 6, 2], // back top notch
                [3, 2, 6, 7], // right top notch

                [4, 8, 9, 5], // left bottom notch
                [5, 9, 10, 6], // back botton notch
                [7, 6, 10, 11], // right bottom notch

                [11, 10, 9, 8], // bottom face

                [0, 3, 7, 11, 8, 4] // front face
            ]
        );
    }
};



module lid_frame(){
   difference(){
   lid(); 
   cube(size = [hole_width,hole_length,hole_depth+10], center = true);
    }
}

lid_frame();
