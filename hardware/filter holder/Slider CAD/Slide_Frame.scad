// Parameterized box generator
// Originally written by Brad Koehn
// Heavily modified by Eric Hankinson to include honeycomb grid on lid, and flatten groove to prevent pushout of box walls.
// Requires OpenSCAD 2014.03 or newer

// box dimensions in millimeters
// typically you want the height < width < length
box_width = 50; // x axis
box_length = 110;  // y axis
box_height = 5; // z axis

// hole dimensions in millimeters

hole_length = 50;
hole_width = 30;
hole_depth = 10;


// thickness of all walls and lid
wall_width = 3;

// how far from the top of the box the lid notch should go
notch_height = 4;

// how deep into the wall the lid notch should go
notch_depth = wall_width / 2;

// diameter of button on lid; Set to 0 for no button
button_diameter = 0;

// height of button on lid
button_height = wall_width / 2;

// how thick walls of the honeycomb are
honeycomb_wall_thickness = 1;

// how thick/deep the honeycomb is; Set to 0 for no honeycomb
// alternatively, you can calculate the height based on the
// notch_height so the honeycomb is no taller than the top of the box
//honeycomb_depth = 2;
honeycomb_depth = notch_height - wall_width * 1.5;

// diameter of each honeycomb
honeycomb_diameter = 10;

// width of the honeycomb pattern
honeycomb_width = box_width - (wall_width * 4);

// length of the honeycomb pattern
honeycomb_length = box_length - (wall_width * 4);


/*
The lid, slightly smaller than the slot in the box. The inset will give just enough that printing
will either require no trimming or little sanding to get the lid to slide easily in the box.
*/

module lid(inset = 0.2) {
    union() {
        polyhedron(
            points = [
                // top points, clockwise starting at max x, max y
                [box_width / 2 - wall_width - inset, box_length / 2, notch_depth], // 0
                [box_width / 2 - wall_width - inset, -(box_length / 2 - wall_width), notch_depth], // 1
                [-(box_width / 2 - wall_width + inset), -(box_length / 2 - wall_width), notch_depth], // 2
                [-(box_width / 2 - wall_width + inset), box_length / 2, notch_depth], // 3

                // middle points, clockwise starting at max x, max y
                [box_width / 2 - wall_width + notch_depth - inset, box_length / 2, -notch_depth], // 4
                [box_width / 2 - wall_width + notch_depth - inset, -(box_length / 2 - wall_width + notch_depth), -notch_depth], // 5
                [-(box_width / 2 - wall_width  + notch_depth + inset), -(box_length / 2 - wall_width + notch_depth), -notch_depth], // 6
                [-(box_width / 2 - wall_width  + notch_depth + inset), box_length / 2, -notch_depth], // 7

                // bottom points, clockwise starting at max x, max y
                [box_width / 2 - wall_width - inset, box_length / 2, -notch_depth], // 8
                [box_width / 2 - wall_width - inset, -(box_length / 2 - wall_width), -notch_depth], // 9
                [-(box_width / 2 - wall_width + inset), -(box_length / 2 - wall_width), -notch_depth], // 10
                [-(box_width / 2 - wall_width + inset), box_length / 2, -notch_depth], // 11
            ],
            faces = [
                [0, 1, 2, 3], // top face

                [0, 4, 5, 1], // left top notch
                [1, 5, 6, 2], // back top notch
                [3, 2, 6, 7], // right top notch

                [4, 8, 9, 5], // left bottom notch
                [5, 9, 10, 6], // back botton notch
                [7, 6, 10, 11], // right bottom notch

                [11, 10, 9, 8], // bottom face

                [0, 3, 7, 11, 8, 4] // front face
            ]
        );
    }
};

module box() {union () {
    translate([0, 0, box_height / 2])
        difference () {
            // our solid box
            cube([box_width, box_length, box_height], center = true);

            // remove the inside
            translate([0, 0, wall_width / 2])
                cube([box_width - wall_width * 2, box_length - wall_width * 2, box_height - wall_width], center = true);

            // remove notch for lid
            translate([0, 0, box_height / 2 - notch_height / 2])
                lid();

            // remove the unnecessary part of box where the lid slides in
            translate([0, wall_width / 2, box_height / 2 ])
                cube([box_width - wall_width * 2, box_length - wall_width, notch_height], center = true);
        };

    }
};



module box_frame(){
   difference(){
   box(); 
   cube(size = [hole_width,hole_length,hole_depth], center = true);
    }
}

box_frame();

