// LED FILTER TESTING RIG: CUVETTE HOLDER
// Designed by Jenny Molloy (jcm80@cam.ac.uk). Copyright Open Bioeconomy Lab 2021, licensed under the CERN OHL-P (https://ohwr.org/cern_ohl_p_v2.txt)
// Adapted from cuvetteholder.scad by Eric Landahl Feb. 21 2020 (https://www.thingiverse.com/thing:4175610/files)

$fn=60;
holderLength = 50;
holderHeight=50;
holderWidth=60;
cuvetteLength=12.7;
cuvetteHeight=45;
cuvetteWidth=12.7;
frameHeight=110;
frameLength=50;
frameDepth=5;
exitLength=10; // thin material at exit of holder
beamRadius=3; // laser beam size
capillaryRadius = 0.75;

//COMPONENT MODULES: ALL VERSIONS//

module holder(){
 cube([holderLength,holderWidth,holderHeight], center=true);
}

module frame_hole(){
 cube([frameDepth,frameLength,holderHeight], center=true);
}

//COMPONENT MODULES: CUVETTE VERSION//
module cuvette_laser_beam_port(){
    rotate([0,90,0])
 cylinder(1.1*holderLength,beamRadius,beamRadius,center=true);
}

module cuvette_port(){
cube([cuvetteLength,cuvetteWidth,cuvetteHeight], center=true);
}

//PARTS MODULES: CUVETTE VERSION//
module cuvette_laser_beam(){
        union(){
        translate([(holderLength-cuvetteLength)/2-exitLength,0,cuvetteHeight/2-(2*beamRadius)]) 
        cuvette_port();
        cuvette_laser_beam_port();
        }
    }
    
module cuvette_holder(){
    difference() {
        holder();
        cuvette_laser_beam();
    }
}

module cuvette_holder_frame(){
difference() {
    cuvette_holder();
    translate([holderLength/2-exitLength/2,0,5]) frame_hole();
    translate([-holderLength/2+exitLength/2,0,5]) frame_hole();
}
}

// CAPILLARY VERSION //

//COMPONENT MODULES: CAPILLARY VERSION//
module capillary(){
 cylinder(h = holderHeight, r1 = 1.1*capillaryRadius, r2 = 1.1*capillaryRadius, center=true);
}

module capillary_laser_beam_port(){
 rotate([0,90,0])
 cylinder(1.1*holderLength,capillaryRadius*2,capillaryRadius*2,center=true);
}

module capillary_laser_beam(){
        union(){
        translate([(holderLength-cuvetteLength)/2-exitLength,0,cuvetteHeight/2-beamRadius]) 
        capillary();
        capillary_laser_beam_port();
        }
    }

//PARTS MODULES: CAPILLARY VERSION//
module capillary_holder(){
    difference() {
        holder();
        capillary_laser_beam();
    }
}

module capillary_holder_frame(){
difference() {
    capillary_holder();
    translate([holderLength/2-exitLength/2,0,5]) frame_hole();
    translate([-holderLength/2+exitLength/2,0,5]) frame_hole();
}
}

// RUN MODULES //

cuvette_holder_frame();